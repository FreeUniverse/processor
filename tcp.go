package main

import (
	ds "common/data_structure"
	"log"
	"net"
	"strings"
	"universe_processor/data_source/jpl_horizons"
)

func startTCPServer(laddr string) {
	listener, err := net.Listen("tcp", laddr)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("獲取Waiter連接錯誤：", err)
		} else {
			go handleConnection(conn)
		}
	}
}

func handleConsultRequest(conn net.Conn, req ds.ConsultRequest) {
	var res ds.ConsultResponse
	for _, t := range req.Target {
		switch strings.ToLower(t) {
		case "planets":
			planets := jpl.AllPlanets()
			res.Planets = planets
		}
	}
	log.Printf("%#v\n", res)
	conn.Write(ds.ToMsg(res))
}

func handlePlanetRequest(conn net.Conn, req ds.PlanetRequest) {
	var tpinfo ds.PlanetResponse
	tpinfo.Time = req.Time
	tpinfo.Planet = jpl.GetInfo(req.Time, req.Name)
	log.Printf("%#v\n", tpinfo)
	conn.Write(ds.ToMsg(tpinfo))
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	re, err := ds.ParseMsg(conn)
	if err != nil {
		return
	}
	switch re.(type) {
	case ds.ConsultRequest:
		handleConsultRequest(conn, re.(ds.ConsultRequest))
	case ds.PlanetRequest:
		handlePlanetRequest(conn, re.(ds.PlanetRequest))
	default:
		log.Printf("無效數據： %v\n", re)
	}
}
