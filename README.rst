Free Universe Processor
=======================

Free Universe的Processor，用來處理/計算星球信息。

* 版本
    0.4.1
* 協議版本
    * TCP式
        0.3.1
    * REST式
        0.2

功能目標
--------
Processor從Waiter處獲取請求，然後根據請求處理/計算出星球信息，再之後返回給Waiter。
