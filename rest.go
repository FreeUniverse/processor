package main

import (
	ds "common/data_structure"
	"common/utils"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"universe_processor/data_source/jpl_horizons"
)

var infoMap map[string]ds.PlanetInfo

func startRESTServer(laddr string) {
	planets := jpl.AllPlanets()
	infoMap = make(map[string]ds.PlanetInfo)
	for _, info := range planets {
		infoMap[info.Name] = info
	}
	http.HandleFunc("/planet_list", hPlanetList) //星球列表
	http.HandleFunc("/info/", hInfo)             //星球信息
	http.HandleFunc("/planet/", hPlanet)         //某時刻（URL下一部分）的星球（再下一部分）信息
	err := http.ListenAndServe(laddr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func hPlanetList(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		planets := make([]string, len(infoMap))
		i := 0
		for k, _ := range infoMap {
			planets[i] = k
			i++
		}
		ret, err := json.Marshal(planets)
		if err != nil {
			panic(err)
		}
		w.Write(ret)
	} else {
		log.Printf("非預期方法。請求：%s %s。來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}

func hInfo(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		path := strings.Split(strings.Trim(req.URL.EscapedPath(), "/"), "/")
		planet := path[1]
		ret, err := json.Marshal(infoMap[planet])
		if err != nil {
			panic(err)
		}
		w.Write(ret)
	} else {
		log.Printf("非預期方法。請求：%s %s。來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}

func hPlanet(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		path := strings.Split(strings.Trim(req.URL.EscapedPath(), "/"), "/")
		time := path[2]
		name := path[1]
		if utils.IsValidTime(time) {
			pInfo := jpl.GetInfo(time, name)
			data, err := json.Marshal(pInfo)
			if err != nil {
				panic(err)
			}
			w.Write(data)
		} else {
			log.Printf("無效時間。請求：%s，來自%s", req.URL)
		}
	} else {
		log.Printf("非預期方法。請求：%s %s，來自%s\n", req.URL, req.Method, req.RemoteAddr)
	}
}
