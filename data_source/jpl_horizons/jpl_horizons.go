package jpl

import (
	"bufio"
	ds "common/data_structure"
	"common/utils"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

const (
	SUFFIX         = ".csv"
	scale  float32 = 10000.0
)

var planets []ds.PlanetInfo
var data map[string]map[string]ds.Coordinate
var done chan bool

var numOfFiles int
var jobdone chan bool
var wlock chan bool

func Init(data_dir []string, info_dir string) {
	data = make(map[string]map[string]ds.Coordinate)
	wlock = make(chan bool, 1)
	wlock <- true
	done = make(chan bool, 1)
	done <- false
	files, err := filepath.Glob(info_dir + "/*.yaml")
	if err != nil {
		panic(err)
	}
	jobdone = make(chan bool, len(files))
	for _, p := range files {
		go read_info(p)
	}
	for _ = range files {
		<-jobdone
	}
	close(jobdone)
	numOfFiles = 0
	for _, dir := range data_dir {
		files, err := filepath.Glob(dir + "/*" + SUFFIX)
		if err != nil {
			panic(err)
		}
		log.Println(files)
		numOfFiles += len(files)
		for _, p := range files {
			go read_file(p)
		}
	}
	jobdone = make(chan bool, numOfFiles)
}

func read_info(filename string) { // 讀取星球信息
	var info ds.PlanetInfo
	fd, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	data := utils.ReadData(fd)
	err = yaml.Unmarshal(data, &info)
	if err != nil {
		panic(err)
	}
	log.Printf("%#v\n", info)
	planets = append(planets, info)
	jobdone <- true
}

func read_file(filename string) {
	fd, err := os.Open(filename)
	if err != nil {
		panic(err) // 文件存在，不當打不開
	}
	defer fd.Close()
	var planetName string
	planetName = filename[strings.LastIndex(filename, "/")+1:]
	planetName = planetName[:len(planetName)-len(SUFFIX)]
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		s := scanner.Text()
		if strings.TrimSpace(s) == "$$SOE" {
			break
		}
	}
	for scanner.Scan() {
		s := scanner.Text()
		if strings.TrimSpace(s) == "$$EOE" {
			break
		}
		strs := strings.Split(s, ",")
		ts := strings.Split(strings.TrimSpace(strs[1]), " ")[1]
		t := strings.Split(ts, "-")
		var m string
		switch t[1] {
		case "Jan":
			m = "01"
		case "Feb":
			m = "02"
		case "Mar":
			m = "03"
		case "Apr":
			m = "04"
		case "May":
			m = "05"
		case "Jun":
			m = "06"
		case "Jul":
			m = "07"
		case "Aug":
			m = "08"
		case "Sep":
			m = "09"
		case "Oct":
			m = "10"
		case "Nov":
			m = "11"
		case "Dec":
			m = "12"
		default:
			panic("不合法的月份")
		}
		time := t[0] + "-" + m + "-" + t[2]
		vstrs := strs[2:5]
		f := func(s string) float32 {
			s = strings.TrimSpace(s)
			num, err := strconv.ParseFloat(s, 32)
			if err != nil {
				panic(err)
			}
			return float32(num)
		}
		coord := ds.NewCoord(f(vstrs[0])/scale, f(vstrs[1])/scale, f(vstrs[2])/scale)
		<-wlock
		if data[time] == nil {
			data[time] = make(map[string]ds.Coordinate)
		}
		data[time][strings.ToLower(planetName)] = coord
		wlock <- true
	}
	jobdone <- true
}

func AllPlanets() []ds.PlanetInfo {
	WaitForDone()
	return planets
}

func WaitForDone() {
	if <-done == true {
		done <- true
		return
	}
	for i := 0; i < numOfFiles; i++ {
		<-jobdone
	}
	done <- true
	log.Println("done")
}

func GetInfo(time, name string) ds.Planet {
	tmd := strings.Split(time, "-")
	if len(tmd[1]) == 1 {
		tmd[1] = "0" + tmd[1]
	}
	if len(tmd[2]) == 1 {
		tmd[2] = "0" + tmd[2]
	}
	time = tmd[0] + "-" + tmd[1] + "-" + tmd[2]
	log.Println(time, name)
	coords, t := data[time]
	if t {
		info, t := coords[strings.ToLower(name)]
		if t {
			return ds.Planet{
				Name:  name,
				Coord: info,
			}
		}
	}
	panic("unknown info")
}
