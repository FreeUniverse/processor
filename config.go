package main

import (
	"common/utils"
	"log"
	"os"
	"strconv"

	"gopkg.in/yaml.v2"
)

type settings struct {
	Port     int      `yaml:"port"`
	Protocal string   `yaml:"protocal,omitempty"`
	DataDir  []string `yaml:"data_dir,omitempty"`
	InfoDir  string   `yaml:"info_dir,omitempty"`
}

func defaultSettings() settings {
	var s settings
	s.Port = 9998
	s.Protocal = "REST"
	s.DataDir = []string{"data"}
	s.InfoDir = "data/info"
	return s
}

func readConfig(filename string) settings {
	s := defaultSettings()
	fd, err := os.Open(filename)
	if err != nil {
		return s
	}
	raw_data := utils.ReadData(fd)
	err = yaml.Unmarshal(raw_data, &s)
	if err != nil {
		log.Fatalf("解析配置文件錯誤。%s\n", err.Error())
	}
	return s
}

func resolveArgs(s settings) settings {
	if len(os.Args) > 1 {
		if os.Args[1] == "-p" {
			if len(os.Args) == 3 {
				port, err := strconv.Atoi(os.Args[2])
				if err != nil {
					panic(err)
				}
				s.Port = port
			} else {
				log.Fatalln("Usage: [-p port]")
			}
		}
	}
	return s
}
