// universe_processor project universe_processor.go
package main

import (
	"strconv"
	"strings"
	"universe_processor/data_source/jpl_horizons"
)

func main() {
	fsett := readConfig("config.yaml")
	sett := resolveArgs(fsett)
	jpl.Init(sett.DataDir, sett.InfoDir)
	laddr := ":" + strconv.Itoa(sett.Port)
	switch strings.ToLower(sett.Protocal) {
	case "rest":
		startRESTServer(laddr)
	case "tcp":
		startTCPServer(laddr)
	}
}
